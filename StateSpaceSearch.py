from heapq import heappush, heappop

# basic functions used in BFS and UCS
def initialNode(state):
    return (state, 0, None)

def removeHead(open):
    return open.pop(0)

def stateNode(node):
    return node[0]

def depthNode(node):
    return node[1]

def gNode(node):
    return node[1]

def hNode(state):
    return heurDict[state]

def fNode(node):
    return gNode(node) + hNode(stateNode(node))

def parentNode(node):
    return node[2]

def isGoalNode(state, goalStates):
    return state in goalStates

def succFun(state):
    statesWeightsLst = stateDict[state]
    statesLst = [x[0] for x in statesWeightsLst]
    return statesLst

def succFunCost(state):
    statesWeightsLst = stateDict[state]
    return statesWeightsLst

def expandNode(node, succ):
    return [(state, depthNode(node) + 1, node) for state in succ(stateNode(node))]

def expandCost(node, succ):
    return [(state, gNode(node)+cost, node) for (state, cost) in succ(stateNode(node))]

def pathNode(node):
    p = parentNode(node)
    if p is None:
        return [stateNode(node)]

    return pathNode(p) + [stateNode(node)]

# basic functions used in astar function - contain f(n) = g(n) + h(state(n)) for the first element (needed for sorting in heap structure)
def initialNode_astar(state):
    return (hNode(state), state, 0, None)

def removeHead_astar(open):
    return heappop(open)

def stateNode_astar(node):
    return node[1]

def expandCost_astar(node, succ):
    return [(gNode_astar(node) + cost + hNode(state), state, gNode_astar(node) + cost, node) for (state, cost) in succ(stateNode_astar(node))]

def gNode_astar(node):
    return node[2]

def parentNode_astar(node):
    return node[3]

def pathNode_astar(node):
    p = parentNode_astar(node)
    if p is None:
        return [stateNode_astar(node)]

    return pathNode_astar(p) + [stateNode_astar(node)]

# implementation of BFS algorithm
def bfs(s0, succ, goal):
    open = [initialNode(s0)]
    visited = set()
    while len(open) != 0:
        n = removeHead(open)

        if isGoalNode(stateNode(n), goal):
            return n, visited

        visited |= {stateNode(n)}

        for m in expandNode(n, succ):

            if stateNode(m) not in visited:
                open.append(m)  #insert back

    return None

# implementation of UCS algorithm
def ucs(s0, succ, goal):
    open = [initialNode(s0)]
    visited = set()
    while len(open) != 0:
        n = removeHead(open)

        if isGoalNode(stateNode(n), goal):
            return n, visited

        visited |= {stateNode(n)}

        for m in expandCost(n, succ):

            if stateNode(m) not in visited:
                open.append(m)  #insert back
                open = sorted(sorted(open, key=stateNode), key=gNode)

    return None

# implementation of A* algorithm
def astar(s0, succ, goal):
    open = {}
    initNode = initialNode_astar(s0)
    open[s0] = initNode
    openLst = []
    heappush(openLst, initNode)
    closed = {}
    while openLst:
        n = removeHead_astar(openLst)

        stateN = stateNode_astar(n)
        if isGoalNode(stateN, goal):
            return n, closed.values()

        closed[stateN] = n

        childNodes = expandCost_astar(n, succ)

        for m in childNodes:
            stateM = stateNode_astar(m)

            if stateM in open:
                openForM = open.get(stateM)
                if openForM:
                    if gNode_astar(openForM) < gNode_astar(m):
                        continue

            open[stateM] = m
            heappush(openLst, m)

    return None

# implementation of heuristics optimism/admissibility check
def isOptimistic(succ, goal):
    # list with elements of format (state, h, h*)
    nonOptimisticList = []

    for state in heurDict:
        h = heurDict[state]
        hStar = astar(state, succ, goal)
        if hStar is None:
            raise Exception("Astar algorithm failed.")

        hStar = hStar[0][2]
        if h > hStar:
            nonOptimisticList.append((state, h, hStar))

    return nonOptimisticList

# implementation of heuristics consistency check
def isConsistent(succ):
    # list with elements of format (state1, h1, state2, h2, c)
    nonConsistentList = []
    for state in stateDict:
        h1 = heurDict[state]
        for succStateCost in succ(state):
            succState = succStateCost[0]
            succCost = succStateCost[1]
            h2 = heurDict[succState]
            c = succCost
            if h1 > h2 + c:
                nonConsistentList.append((state, h1, succState, h2, c))

    return nonConsistentList

# helper methods
def printPath(path):
    pathLastInd = len(path)-1
    for i in range(pathLastInd):
        print("{} =>".format(path[i]))
    print(path[pathLastInd])

def runBFS(initState, goalStates):
    print()
    print("Running bfs:")
    nodeEnd, visited = bfs(initState, succFun, goalStates)
    closed = visited | {stateNode(nodeEnd)}
    pathNodeEnd = pathNode(nodeEnd)
    print("States visited = "+str(len(closed)))
    print("Found path of length: " + str(len(pathNodeEnd)))
    printPath(pathNodeEnd)

def runUCS(initState, goalStates):
    print()
    print("Running ucs:")
    nodeEnd, visited = ucs(initState, succFunCost, goalStates)
    closed = visited | {stateNode(nodeEnd)}
    pathNodeEnd = pathNode(nodeEnd)
    print("States visited = " + str(len(closed)))
    print("Found path of length {} with total cost {}:".format(len(pathNodeEnd), gNode(nodeEnd)))
    printPath(pathNodeEnd)

def runAstar(initState, goalStates, heurName):
    print()
    print("Running astar + heuristics "+heurName+":")
    result = astar(initState, succFunCost, goalStates)
    if result is None:
        raise Exception("Astar algorithm failed.")
    nodeEnd, visited = result
    closed = set(visited) | {stateNode_astar(nodeEnd)}
    pathNodeEnd = pathNode_astar(nodeEnd)
    print("States visited = " + str(len(closed)))
    print("Found path of length {} with total cost {}:".format(len(pathNodeEnd), gNode_astar(nodeEnd)))
    printPath(pathNodeEnd)

# loading data from state space descriptor file
def loadData(stateSpaceDescr):
    states = []
    global stateDict
    stateDict = {}

    for line in stateSpaceDescr:
        if line.startswith("#"):
            continue

        if ":" not in line:
            states.append(line.strip())
            continue

        states.append(line[:line.find(":")].strip())

        currentAndSuccessors = line.split()
        current = currentAndSuccessors[:1][0][:-1]
        successors = currentAndSuccessors[1:]

        stateDict[current] = []
        for succ in successors:
            targetState, transCost = succ.split(",")
            transCost = float(transCost)
            stateDict[current].append([targetState, transCost])

    initState = states[0]
    goalStates = states[1].split()

    statesCpy = states[:]
    statesSet = set()
    for el in statesCpy:
        statesEl = el.split()
        for stateEl in statesEl:
            statesSet.add(stateEl)

    states = statesSet

    #print(stateDict)
    print("Start state: " + initState)
    print("End state(s): " + str(goalStates))
    print("State space size: " + str(len(states)))
    print("Total transitions: " + str(sum([len(el) for el in stateDict.values()])))

    return initState, goalStates

# loading data from heuristics descriptor file (used in A*)
def loadHeuristics(heuristics1Descr):
    global heurDict
    heurDict = {}
    for line in heuristics1Descr:
        if line.startswith("#"):
            continue

        state, heurVal = line.split()
        state = state[:-1]
        heurVal = float(heurVal)

        heurDict[state] = heurVal

    # print(heurDict)

def checkHeuristicAdmissibility(goalStates):
    print("Checking if heuristic is optimistic.")
    nonOptimisticList = isOptimistic(succFunCost, goalStates)
    if not nonOptimisticList:
        print("Heuristic is optimistic")
    else:
        if len(nonOptimisticList) > 50:
            print("  [ERR] {} errors, omitting output.".format(len(nonOptimisticList)))
            print("Heuristic is not optimistic")
            return
        for nonOptimisticState in nonOptimisticList:
            print("  [ERR] h({}) > h*: {} > {}".format(nonOptimisticState[0], nonOptimisticState[1],
                                                       nonOptimisticState[2]))
        print("Heuristic is not optimistic")

def checkHeuristicConsistency():
    print("Checking if heuristic is consistent.")
    nonConsistentList = isConsistent(succFunCost)
    if not nonConsistentList:
        print("Heuristic is consistent")
    else:
        if len(nonConsistentList) > 50:
            print("  [ERR] {} errors, omitting output.".format(len(nonConsistentList)))
            print("Heuristic is not consistent")
            return
        for nonConsistentState in nonConsistentList:
            print("  [ERR] h({}) > h({}) + c: {} > {} + {}".format(nonConsistentState[0], nonConsistentState[2],
                                                                   nonConsistentState[1], nonConsistentState[3],
                                                                   nonConsistentState[4]))
        print("Heuristic is not consistent")

# parameters checkAdmissibility and checkConsistency set to True, except for A* where admissibility check takes too much time so it is not called (checkAdmissibility=False)
def checkHeuristic(goalStates, checkAdmissibility, checkConsistency):
    print("------")
    print("Checking heuristic")
    if checkAdmissibility:
        checkHeuristicAdmissibility(goalStates)
    print()
    if checkConsistency:
        checkHeuristicConsistency()


if __name__=="__main__":
    basePath = "./stateSpaceFiles/"
    print("#####################################################################################################################################")
    print("----- Passing the AI course -----")
    with open(basePath+"ai.txt", "r", encoding="utf8") as stateSpaceDescr:
        initState, goalStates = loadData(stateSpaceDescr)

    runBFS(initState, goalStates)
    runUCS(initState, goalStates)

    heur1name = "ai_fail.txt"
    with open(basePath+heur1name, "r", encoding="utf8") as heuristics1Descr:
        loadHeuristics(heuristics1Descr)

    runAstar(initState, goalStates, heur1name)

    checkHeuristic(goalStates, True, True)

    print("------")

    heur2name = "ai_pass.txt"
    with open(basePath+heur2name, "r", encoding="utf8") as heuristics2Descr:
        loadHeuristics(heuristics2Descr)

    runAstar(initState, goalStates, heur2name)

    checkHeuristic(goalStates, True, True)

    print()
    print("#####################################################################################################################################")
    print("----- Trip to Buzet -----")
    with open(basePath+"istra.txt", "r", encoding="utf8") as stateSpaceDescr:
        initState, goalStates = loadData(stateSpaceDescr)

    runBFS(initState, goalStates)
    runUCS(initState, goalStates)

    heur1name = "istra_heuristic.txt"
    with open(basePath+heur1name, "r", encoding="utf8") as heuristics1Descr:
        loadHeuristics(heuristics1Descr)

    runAstar(initState, goalStates, heur1name)

    checkHeuristic(goalStates, True, True)

    print("------")

    heur2name = "istra_pessimistic_heuristic.txt"
    with open(basePath+heur2name, "r", encoding="utf8") as heuristics2Descr:
        loadHeuristics(heuristics2Descr)

    runAstar(initState, goalStates, heur2name)

    checkHeuristic(goalStates, True, True)

    print()
    print("#####################################################################################################################################")
    print("----- 8-puzzle -----")

    with open(basePath+"3x3_puzzle.txt", "r", encoding="utf8") as stateSpaceDescr:
        initState, goalStates = loadData(stateSpaceDescr)

    heur1name = "3x3_misplaced_heuristic.txt"
    with open(basePath+heur1name, "r", encoding="utf8") as heuristics1Descr:
        loadHeuristics(heuristics1Descr)

    runAstar(initState, goalStates, heur1name)

    # admissibility check is here turned off because it takes unreasonable amount of time to calculate (> 5 min)
    checkHeuristic(goalStates, False, True)